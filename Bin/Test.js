const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');

client.once('ready', () => {
	console.log('Bot Online.');
});

client.on("guildMemberAdd", (member) => {
  if (!config.greet == "") {
    const guild = member.guild;
    if (!newUsers[guild.id]) newUsers[guild.id] = new Discord.Collection();
    newUsers[guild.id].set(member.id, member.user);

    if (newUsers[guild.id].size > 0) {
          const userlist = newUsers[guild.id].map(u => u.toString()).join(" ");
          guild.channels.find(channel => channel.name === "general").send(config.greet);
          newUsers[guild.id].clear();
    }
  }
});

client.on("guildMemberRemove", (member) => {
  if (!config.greet == "") {
    const guild = member.guild;
    if (newUsers[guild.id].has(member.id)) newUsers.delete(member.id);
  }
});


client.on('message', message => {
            //Admin Commands
            if (message.author.id === config.adminid) {
    	        if (message.content.startsWith(config.prefix + "test")) {
                    message.channel.send("Test Success!");
                }
                if (message.content.startsWith(config.prefix + "kick")) {
                        const user = message.mentions.users.first();
                        let mentionMember = message.mentions.members.first();
                        const member = message.guild.member(user);
                        mentionMember.kick()
                        member.kick("Kicked By A Bot");
                        message.channel.send("Executed.");
                }
                if (message.content.startsWith(config.prefix + "ban")) {
                        const user = message.mentions.users.first();
                        message.guild.members.ban(user);
                        message.channel.send("Executed.");
                }
                if (message.content.startsWith(config.prefix + "exit")) {
                        process.exit()
                }
            }
            //User Commands
});

client.login(config.token);